#!/usr/bin/env python3

import json
import xmltodict
from zeep import Client
from pprint import pprint

API_URL = 'http://apps.metrosp.com.br/api/diretodometro/v1/SituacaoLinhasMetro.asmx?WSDL'
API_KEY = 'B7758201-15AF-4246-8892-EAAFFC170515'

client = Client(API_URL)
result = client.service.GetSituacaoTodasLinhas(API_KEY)
diretodometro = json.loads(json.dumps(xmltodict.parse(result)))['diretodometro']

for linha in diretodometro['linhas']['linha']:
    print(linha['nome'],linha['situacao'])
